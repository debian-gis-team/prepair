Source: prepair
Section: science
Priority: optional
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Bas Couwenberg <sebastic@debian.org>
Build-Depends: debhelper (>= 9),
               cmake,
               libcgal-dev,
               libgdal-dev,
               docbook2x,
               docbook-xsl,
               docbook-xml,
               xsltproc
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/debian-gis-team/prepair/
Vcs-Git: https://salsa.debian.org/debian-gis-team/prepair.git
Homepage: https://github.com/tudelft3d/prepair

Package: prepair
Architecture: any
Depends: prepair-data,
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: pprepair
Description: polygon repair tool
 prepair permits you to easily repair "broken" GIS polygons, and that
 according to the international standards ISO 19107. In brief, given a
 polygon stored in WKT, it automatically repairs it and gives you back
 a valid WKT. Automated repair methods can be considered as interpreting
 ambiguous or ill-defined polygons and giving a coherent and clearly
 defined output.

Package: prepair-data
Architecture: all
Depends: ${misc:Depends}
Description: polygon repair tool -- example data
 prepair permits you to easily repair "broken" GIS polygons, and that
 according to the international standards ISO 19107. In brief, given a
 polygon stored in WKT, it automatically repairs it and gives you back
 a valid WKT. Automated repair methods can be considered as interpreting
 ambiguous or ill-defined polygons and giving a coherent and clearly
 defined output.
 .
 This package contains the architecture independent example data.
